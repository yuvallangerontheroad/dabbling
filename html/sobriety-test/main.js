(function() {
    function isLowercase(c) {
        return (c >= "a" && c <= "z");
    };

    function isUppercase(c) {
        return (c >= "A" && c <= "Z");
    };

    function toggleCase(c) {
        // This whole thing assumes ASCII.  Sorry.
        if (isLowercase(c)) {
            return c.toUpperCase();
        } else if (isUppercase(c)) {
            return c.toLowerCase();
        } else {
            return c;
        };
    };

    function randint(a, b) {
        return Math.floor(
            (b - a) * Math.random() + a
        );
    };

    function isEnglishCodePoint(c) {
        return (isLowercase(c) || isUppercase(c));
    }

    function countEnglishCodePoints(ourArray) {
        let count = 0;
        ourArray.forEach((c)=>{
            if (isEnglishCodePoint(c))
                count += 1;
        });
        return count;
    }

    function chooseRandomEnglishCodePointFromString(ourString, numberOfPositionsToChoose) {
        let ourStringAsArray = Array.from(ourString);

        if (ourStringAsArray.length < numberOfPositionsToChoose) {
            throw new Error("Input string shorter that ${numberOfPositionsToChoose}");
        }
        if (countEnglishCodePoints(ourStringAsArray) < numberOfPositionsToChoose) {
            throw new Error("Not enough English letters in input.");
        }


        let chosenPositions = [];

        while (chosenPositions.length < numberOfPositionsToChoose) {
            let theRandomPosition = randint(0, ourStringAsArray.length);
            let theRandomPositionValue = ourStringAsArray[theRandomPosition];
            if (isEnglishCodePoint(theRandomPositionValue)
                && !(chosenPositions.includes(theRandomPosition))
               ) {
                chosenPositions.push(theRandomPosition);
            }
        }

        return chosenPositions;
    };

    function toggleStringCaseAccordingToPositions(ourString, ourPositions) {
        return Array.from(ourString).map(
            (c, i) =>
            ourPositions.includes(i)
                ? toggleCase(c)
                : c
        ).join("");
    };

    function getUppercase(ourString) {
        return Array.from(
            ourString
        ).filter(
            isUppercase
        ).join("");
    };

    function click_handler(x) {
        let input_element = document.getElementById("input");
        let test_element = document.getElementById("test");

        if (getUppercase(test_element.textContent) === input_element.value) {
            alert("PASSED");
        } else {
            alert("LAY OFF THE BOOZE, יא שתיין!");
        }
    };

    function scrambleElementTextContent(obj) {
        let ourString = obj.textContent;

        let indicesToToggle = chooseRandomEnglishCodePointFromString(
            ourString,
            4,
        );

        let toggledString = toggleStringCaseAccordingToPositions(
            ourString,
            indicesToToggle,
        );

        obj.textContent = toggledString;
    };

    function main() {
        scrambleElementTextContent(document.getElementById("test"));

        let check_sobriety_button = document.getElementById("button");
        check_sobriety_button.addEventListener(
            "click",
            click_handler,
        );
    };

    window.addEventListener('load', main);
})();
